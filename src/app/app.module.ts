import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './modules/app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AdminComponent,NavbarComponent,SidebarComponent,
            ContentComponent,ContentDetailComponent,ContentCreateComponent,ContentBoxHeaderComponent,
            TableComponent } from './admin';

import { UserComponent,UserDetailComponent,UserCreateComponent,HomeComponent } from './admin';

import { LoginComponent,RegisterComponent,AuthGuard } from './auth';

import {AuthenticationService,UserService} from './services';

@NgModule({
  declarations: [
    AppComponent,

    AdminComponent,NavbarComponent,SidebarComponent,

    ContentComponent,ContentCreateComponent,ContentDetailComponent,ContentBoxHeaderComponent,

    TableComponent,

    UserComponent,UserDetailComponent,UserCreateComponent,

    HomeComponent,

    LoginComponent,RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
        AuthGuard,
        AuthenticationService,
        UserService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
