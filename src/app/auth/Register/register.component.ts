import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {User} from '../../models';

import {  UserService } from '../../services';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit{
    model: any = {};
    loading = false;
    currentUser:User;
    returnUrl:any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService) {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
    ngOnInit() {
            // get return url from route parameters or default to '/'
            this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
            // redirectTo
            if(this.currentUser != null){
                this.router.navigate([this.returnUrl]);

            }

        }

    register() {
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    // this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    // this.alertService.error(error);
                    this.loading = false;
                });
    }
}
