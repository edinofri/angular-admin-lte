import { HomeComponent,
    UserComponent,UserDetailComponent,UserCreateComponent } from './../admin';
import { LoginComponent,RegisterComponent} from './../auth';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth';

@NgModule({
  imports: [
    RouterModule.forRoot([
        { path: '', redirectTo: 'administrator', pathMatch: 'full' },
        { path: 'administrator', component: HomeComponent ,canActivate: [AuthGuard] },

        { path: 'administrator/user', component: UserComponent ,canActivate: [AuthGuard] },
        { path: 'administrator/user/view/:id', component: UserDetailComponent ,canActivate: [AuthGuard] },
        { path: 'administrator/user/add', component: UserCreateComponent ,canActivate: [AuthGuard] },
        { path: 'administrator/user', component: UserComponent ,canActivate: [AuthGuard] },

        { path: 'login', component: LoginComponent },
        { path: 'register', component: RegisterComponent },

    ])
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
