import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        return this.http.post<any>('http://resep.project.air/api/user/login', { eu: username, password: password })
            .map(result => {
                // login successful if there's a jwt token in the response
                if (result.success) {
                    console.log(result.data.user);
                    localStorage.setItem('currentUser', JSON.stringify(result.data.user));
                }
                return result;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
