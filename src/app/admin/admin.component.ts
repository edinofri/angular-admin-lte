import { Component,Input } from '@angular/core';
import { User} from '../models';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
})
export class AdminComponent{
    @Input() tag:string;
    @Input() description:string;

    currentUser: User;

    constructor(
        private route: ActivatedRoute,
        private router: Router) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    logout(){
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }

}
