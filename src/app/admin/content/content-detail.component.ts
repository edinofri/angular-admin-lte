import { Component,Input,OnChanges } from '@angular/core';
@Component({
  selector: 'app-content-detail',
  templateUrl: './content-detail.component.html',
})
export class ContentDetailComponent implements OnChanges{
    @Input() data:any;
    @Input() tag:string;
    @Input() path:string;
    keys:string[] = [];
    ngOnChanges(){
        if(this.data != null){
            this.keys = Object.keys(this.data);
        }
    }
        }

    }
}
