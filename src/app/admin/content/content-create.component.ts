import { Component,Input } from '@angular/core';
@Component({
  selector: 'app-content-create',
  templateUrl: './content-create.component.html',
})
export class ContentCreateComponent implements OnChanges{
    @Input() tag:string;
    @Input() path:string;
    keys:string[] = [];

}
