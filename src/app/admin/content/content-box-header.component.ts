import { Component,Input } from '@angular/core';

@Component({
  selector: 'app-content-box-header',
  templateUrl: './content-box-header.component.html',
})
export class ContentBoxHeaderComponent{
    @Input() tag:string;
    @Input() total:number;
    @Input() path:string;
    @Input() detail:boolean;
}
