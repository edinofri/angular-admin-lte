import { Component,Input} from '@angular/core';
import { User} from '../../models';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent{
    @Input() currentUser:User = {};
}
