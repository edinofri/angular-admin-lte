import { Component,Input, OnInit} from '@angular/core';
import { UserService } from '../../../services';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
})

export class UserDetailComponent implements OnInit {
    constructor(private userService: UserService) {}
    data:any;
    ngOnInit(){
        this.userService.getById(5).subscribe(
            data => {
                this.data = data;
            },
            error => {

            });
    }
}
