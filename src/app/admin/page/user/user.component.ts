import { Component,Input, OnInit} from '@angular/core';
import { UserService } from '../../../services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {
    constructor(private userService: UserService) {}
    total:number;
    sampleTitles:string[] = ['Username','Name','Email','Phone','Created At','Updated At'];
    sampleData = [];

    ngOnInit(){
        this.userService.getAll().subscribe(
            data => {
                this.sampleData = data.items;
                this.total = data.total;
            },
            error => {

            });
    }
}
