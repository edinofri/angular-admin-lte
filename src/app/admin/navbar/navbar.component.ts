import { Component,Input} from '@angular/core';
import { User } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent{
    @Input() currentUser:User = {};

    constructor(
        private router: Router) {
    }

    logout(){
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }
}
