import { Component,Input,OnChanges} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
})
export class TableComponent implements OnChanges{
    @Input() titles:string[] = [];
    @Input() items:any[] = [];
    @Input() path:string;

    keys:string[] = [];

    ngOnChanges(){
        if(this.items.length > 0){
            this.keys = Object.keys(this.items[0]);
            this.keys.shift();
        }
    }
}
